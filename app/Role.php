<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

  #in case if you want to do inverse
    public function users(){

      return $this->belongsToMany('App\User');

    }

}
