<?php
use App\Post;
use App\Tag;
use App\Photo;
use App\Country;
use App\User;
use App\Role;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*
Route::get('/admin/post/example', array('as'=>'admin.home' ,function(){

  $url = route('admin.home');
  return "this is above url". $url;

  }));
*/
//Route::get('/post/{id}', 'PostController@index');
//Route::resource('post', 'PostController');
/*
Route::get('/delete', function(){

  $deleted = DB::delete('delete from post where id=?', [1]);
  return $deleted;

});


Route::get('/update',function(){

  $updated = DB::update('update post set title="updated title" where id=?', [1]);
  return $updated;


});
*/

/*
Route::get('/read', function(){

  $result =  DB::select('select * from post where id=?', [1]);
  foreach ($result as $post) {
    #dd($post);
    return  $post->title;
    #return $post->content;
  }

  });
*/

/*
Route::get('/insert', function(){
  DB::insert('insert into posts(title, content) values(?, ?)', [' laravl', ' php is the best']);

});
*/
/*
Route::get('/test', 'testController@test');

#Route::get('/test', 'PostController@test');

Route::get('/contact', 'PostController@contact');

Route::get('/post/{id}/{name}', 'PostController@post');
*/

#Eloquent
/*Route::get('/read', function(){

  #if u want to access only one item then
  #$read = Post::find(id_no)
  #return $read->name_of_field_u_want_to_show
  $read = Post::all();
  foreach($read as $see){
    return $see->title;
  }


});
*/
/*
Route::get('/findwhere', function(){

  $posts = Post::where('id',2)->orderBy('id', 'desc')->take(1)->get();
  #dd($posts);
  return $posts;


});
*/

/*
Route::get('/findmore', function(){

  $posts = Post::findOrfail(1);
  return $posts;


});
*/
#to insert into database
/*
Route::get('/basicinsert', function(){

  $post = new Post;
  $post->title = "Hello this is Eloquent insert";
  $post->content = "waooooo Eloquent is awesome";

  $post->save();

});
*/


#to update the database content

/*Route::get('/basicupdate', function(){

  $post = Post::find(2);

  $post->title = "Eloquent";
  $post->content = "Eloquent";
  $post->save();


});
*/
/*
Route::get('/create', function(){

  Post::create(['title'=>'Creating title from Eloquent', 'content'=>'this is the Eloquent create method']);


});
*/
/*
Route::get('/update', function(){

  Post::where('id',2)->update(['title'=>'New update php title', 'content'=>'New fresh content']);

});
*/
/*
Route::get('/delete', function(){

  $post = Post::find(1);
  $post->delete();


});

Route::get('/delete2', function(){

  Post::destroy(3);
  Post::destroy([4,5]);
  #Post::where('is_admin', 0)->delete();

});

Route::get('/softdelete', function(){

  Post::find(3)->delete();


});

Route::get('/readsoftdelete', function(){

  $read = Post::withTrashed()->where('is_admin',0)->get();
  return $read;

});

Route::get('/restore', function(){

  Post::withTrashed()->where('is_admin', 0)->restore();


});

Route::get('/forcedelete', function(){

  Post::onlyTrashed()->where('is_admin', 0)->forceDelete();

});

*/
############################################################
############################################################

#Eloquent Relationship

#One to One Relationship
/*
Route::get('/user/{id}/post', function($id){

  return User::find($id)->post;


});


Route::get('/post/{id}/user', function($id){

  return Post::find($id)->user->name;


});

#One to many Relationship

Route::get('/posts', function(){

  $user = User::find(1);
  foreach($user->posts as $post){
    #dd($post->title);
    echo $post->title ."<br>";

  }

});

#Many to many Relationship

Route::get('/user/{id}/role', function($id){

  $user = User::find($id);
  #$dd($user);
  foreach($user->roles as $role)
    return $role->name;


});

# Accessing intermediate table / pivot

Route::get('/user/pivot', function(){

  $user = User::find(1);
  foreach($user->roles as $role)
    return $role->created_at;

});

Route::get('/country/user', function(){

  $country = Country::find(2);
    foreach ($country->posts as $post) {
      // code...
      return $post->title;
    }

});

#Polymorphic Relationship

Route::get('/user/photos', function(){

  $user = User::find(1);
    foreach($user->photos as $photo){
      return $photo->path;
    }

});
*/
/*

Route::get('/photo/{id}/post', function($id){

  $photo = Photo::findOrfail($id);
    return $photo->imageable;

});


Route::get('/post/tag', function(){

  $post = Post::find(2);
    foreach($post->tags as $tag){
      // code...
      return $tag;
    }

});

Route::get('/tag/post', function(){

  $tag = Tag::find(2);
    #dd($tag);
    foreach($tag->posts as $post){

      return $post->title;

    }

});
*/

/*
############################################################
    CRUD Application
############################################################
*/


Route::group(['middleware'=>'web'], function(){

    Route::resource('/posts', 'PostsController');


    Route::get('/dates', function(){

      $date = new DateTime();

      echo $date->format('m-d-y');

      echo '<br>';

      echo Carbon::now();

      echo '<br>';

      echo Carbon::now()->yesterday()->diffForHumans();


    });



});

Route::get('/getname', function(){

  $user = User::find(1);

  echo $user->name;

});

Route::get('/setname', function(){

  $user = User::find(1);

  $user->name = 'infanite';

  $user->save();


});

Route::auth();

Route::get('/home', 'HomeController@index');
