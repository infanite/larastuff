<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      return view('posts.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {

      $file = $request->all();

      if($file = $request->file('file')){

        $name = $file->getClientOriginalName();

        $file->move('images', $name);

        $input['path'] = $name;

      }

      Post::create($input);


      // $file = $request->file('file');
      //
      // echo $file->getClientOriginalName();
      //
      // echo '<br>';
      //
      // echo $file->getClientSize();

      // $this->validate($request, [
      //
      //   'title'=>'required',
      //   'content'=>'required'
      //
      //
      // ]);//yaha semicolon nalekhda tala "unexpected post vanera error dekhaox..soo error k ma dekhako x tesko mathi pani hernu parx..syntax haru missing huna sakx"
      //
      // Post::create($request->all());
      //
      // return redirect('/posts');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $post = Post::findOrfail($id);

        return view('posts.show', compact('post'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $post = Post::findOrfail($id);

        return view('posts.edit', compact('post'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // return request->all();
        // return "its working";
        $post = Post::findOrfail($id);

        $post->update($request->all());

        return redirect('/posts');
        // return redirect('posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::whereId($id)->delete();

        return redirect('/posts');

    }
}
